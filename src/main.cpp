#include "grafy.hh"
#include "grafy_tablica.hh"
#include "grafy_lista.hh"
#include "timer.h"
#include <string>
#include <vector>
#include <fstream>
#define INF 1000000000

std::vector<std::string> FindPath(std::string vert1, std::string vert2, tab::Graph &graph)
{
  std::vector<std::string> path;
  tab::Vert** prev = new tab::Vert*[graph._size];
  int* dist = new int[graph._size];
  bool change = 0;
  int val, size;
  tab::Vert* temp1, *temp2;

  size = graph._size;
  if(size == 0)
    return path;
  for(int i = 0; i < size; i++)
  {
    dist[i] = INF;
    prev[i] = NULL;
  }
  dist[((tab::Vert*) graph.find(vert1))->key] = 0;
 
  for(int i = 0; i < graph._size - 1; i++)
  {
    for(int j = 0; j < graph._edge.size(); j++)
    {
      val = graph._edge[j]->value();
      temp1 = (tab::Vert*)graph._edge[j]->_vert[0];
      temp2 = (tab::Vert*)graph._edge[j]->_vert[1];
      
      if(dist[temp1->key] + val < dist[temp2->key])
      {
	dist[temp2->key] = dist[temp1->key] + val;
	prev[temp2->key] = temp1;
	change=1;
      }
      else if(dist[temp2->key] + val < dist[temp1->key])
      {
	dist[temp1->key] = dist[temp2->key] + val;
	prev[temp1->key] = temp2;
	change=1;
      }
    }
    
    if(!change)
      break;
    change = 0;
  }

  temp1 = (tab::Vert*) graph.find(vert1);
  temp2 = (tab::Vert*) graph.find(vert2);

  while(temp2!=temp1)
  {
    path.push_back(temp2->name());
    temp2 = prev[temp2->key];
  }
  path.push_back(temp1->name());
		    
  return path;
}

int find(std::string name, list::Graph &graph)
{
  int size = graph.size();
  int i;
  for(i=0; i<size; i++)
  {
    if(!name.compare(graph._vert[i]->name()))
      break;
  }
  return i;
}

std::vector<std::string> FindPath(std::string vert1, std::string vert2, list::Graph &graph)
{
  int val, size = graph.size();
  std::vector<std::string> path;
  list::Vert** prev = new list::Vert*[size];
  int* dist = new int[size];
  bool change = 0;
  list::Vert *temp1, *temp2;

  if(size == 0)
    return path;
  
  for(int i = 0; i < size; i++)
  {
    dist[i] = INF;
    prev[i] = NULL;
    ((list::Vert*)graph._vert[i])->key = i;
  }
  dist[find(vert1, graph)] = 0;

  for(int i = 0; i < size - 1; i++)
  {
    for(int j = 0; j < graph._edge.size(); j++)
    {
      val = graph._edge[j]->value();
      temp1 = (list::Vert*)graph._edge[j]->_vert[0];
      temp2 = (list::Vert*)graph._edge[j]->_vert[1];
      
      if(dist[temp1->key] + val < dist[temp2->key])
      {
	dist[temp2->key] = dist[temp1->key] + val;
	prev[temp2->key] = temp1;
	change=1;
      }
      else if(dist[temp2->key] + val < dist[temp1->key])
      {
	dist[temp1->key] = dist[temp2->key] + val;
	prev[temp1->key] = temp2;
	change=1;
      }
    }
    
    if(!change)
      break;
    change = 0;
  }

  temp1 = (list::Vert*) graph.find(vert1);
  temp2 = (list::Vert*) graph.find(vert2);
  
  while(temp2!=temp1)
  {
    path.push_back(temp2->name());
    temp2 = prev[temp2->key];
  }
  path.push_back(temp1->name());
  
  return path;
}

int main()
{
  list::Graph graf_list;
  tab::Graph* graf_tab[5]; 
  Timer timer;
  std::ofstream out;
  
  double density[] = {0.25, 0.5, 0.75, 1};
  int size[] = {10, 50, 100, 200, 300};
  double sum;

  out.open("out.csv");
  out << "lista [us]\nn,10,50,100,200,300\n";
  for(int i=0; i<0; i++)   //density
  {
    out << (int)(density[i]*100) << '%';
    for(int j=0; j<5; j++) //size
    {
      for(int k=0; k<100; k++)
      {
	graf_list.fill(density[i], size[j]);
	timer.start();
	FindPath(graf_list._vert[rand()%graf_list._vert.size()]->name(), graf_list._vert[rand()%graf_list._vert.size()]->name(), graf_list);
	timer.stop();
	sum=sum+timer.getElapsedTimeInMicroSec();
	}
      out << ',' << sum/100;
      sum=0;
    }
    out << '\n';
  }

  for(int i=0; i<5; i++)
    graf_tab[i] = new tab::Graph(size[i]);

  out << "\n\ntablica [us]\nn,10,50,100,200,300\n";
  for(int i=0; i<4; i++)   //density
  {
    out << (int)(density[i]*100) << '%';
    for(int j=0; j<5; j++) //size
    {
      for(int k=0; k<100; k++)
      {
	graf_tab[j]->fill(density[i]);
	timer.start();
	path = FindPath(graf_tab[j]->_vert[rand()%graf_tab[j]->_vert.size()]->name(), graf_tab[j]->_vert[rand()%graf_tab[j]->_vert.size()]->name(), *(graf_tab[j]));
	timer.stop();
	sum=sum+timer.getElapsedTimeInMicroSec();
	}
      out << ',' << sum/100;
      sum=0;
    }
    out << '\n';
  }
  
  out.close();
  return 0;
}
